# Feature Request
## Feature Type
Select with an x in the "[]"
- [] Assembly Selection
- [] Display Enhancement
- [] New Analysis
- [] Species Scheme
- [] Other

## Describe
{Let us know what you want to be able to do or what we can improve.}