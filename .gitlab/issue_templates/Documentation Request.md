# Documentation Request
## Type
Put x in relevant `[]`:
- [x] Missing documentation
- [] Incorrect or confusing
- [] Other

## Summary
{Describe what could be improved or is missing.}

## Documentation URL
{If possible, please include the URL of the relevant page in our [Documentation](https://cgps.gitbook.io/pathogenwatch/).}

## Other Notes