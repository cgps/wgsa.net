# Result Error
## Type
Put "x" in the appropriate box
- [] Missing result
- [] Incorrect result

## Name of tool
{e.g. MLST, or PAARSNP}

## Brief Description
{Please give the expected result and why, along with the incorrect result}

## Example
{Please give the identifier of an example assembly or URL of the collection. Alternatively attach a zipped FASTA file to the report.}
