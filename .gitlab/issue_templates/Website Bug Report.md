# Website Bug Report
## Bug Type
Select with an x in the "[]"
- [] Broken function
- [] Display Problem
- [] Other

{Delete description text in brackets below}

## Summary
{A brief description of the error, and what was expected to happen}

## What is the current bug behavour?
{What are you seeing? If possible, attach a screenshot.}

## What is the expected behaviour?
{What would you expect to see?}

## Steps to replicate
{Please describe step by step what you are doing to help us replicate it.}

## URL of page with error
{If possible provide a link to the page where the problem is.}

## System Information
### Logged in
- [] Yes
- [] No

### Browser (and version)
{e.g. Chrome 66. This can be found in `Help/About`.}

### Operating System
{e.g. macOS 10.13.4}
